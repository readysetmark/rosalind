
(ns rosalind.test
	(:require [rosalind.dna :as dna])
	(:require [rosalind.rna :as rna])
	(:use clojure.test))

(testing "dna"
	(deftest test-count-nucleotides
		(let [dna1 "ACGT"
			  dna2 "AAAA"
			  dna3 "CCCC"
			  dna4 "GGGG"
			  dna5 "TTTT"
			  dna6 "AGCTTTTCATTCTGACTGCAACGGGCAATATGTCTCTGTGTGGATTAAAAAAAGAGTGTCTGATAGCAGC"]
			(are [dna c] (= c (dna/count-nucleotides dna))
				dna1 {\A 1 \C 1 \G 1 \T 1}
				dna2 {\A 4 \C 0 \G 0 \T 0}
				dna3 {\A 0 \C 4 \G 0 \T 0}
				dna4 {\A 0 \C 0 \G 4 \T 0}
				dna5 {\A 0 \C 0 \G 0 \T 4}
				dna6 {\A 20 \C 12 \G 17 \T 21}))))

(testing "rna"
	(deftest test-transcribe-dna-to-rna
		(is (= "GAUGGAACUUGACUACGUAAAUU" (rna/transcribe-dna-to-rna "GATGGAACTTGACTACGTAAATT")))))
