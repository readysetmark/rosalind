(defproject rosalind/rosalind "0.0.1-ALPHA"
	:description "Solutinos to Rosalind problems"
	:url "http://rosalind.info/problems/list-view/"
	:dependencies [[org.clojure/clojure "1.4.0"]
				   [org.clojure/math.combinatorics "0.0.1"]]
	:source-paths ["src" "src/rosalind"]
	:test-paths ["test" "test/rosalind"])
