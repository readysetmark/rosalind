; Mark Williams
; 2012
;
; Rosalind - PERM
; Enumerating Gene Orders

(ns rosalind.perm
	(:require [clojure.math.combinatorics :as comb])
	(:use [clojure.string :only [join]]))

(defn positive-integers [n]
	(rest (range (inc n))))

(defn permutations [n]
	(comb/permutations (positive-integers n)))

(defn print-permutations [n]
	(let [perms (permutations n)]
		(println (count perms))
		(println (join "\r\n" (map #(join " " %) perms)))))
