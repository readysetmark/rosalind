; Mark Williams
; 2012
;
; Rosalind - DNA
; Counting Nucleotides

(ns rosalind.dna
	(:use [clojure.string :only [join]]))

(defn count-nucleotides [dnastring]
	(letfn [(sumchars 
				[counts char]
				(assoc counts char (inc (get counts char))))]
		(reduce sumchars {\A 0 \C 0 \G 0 \T 0} dnastring)))

(defn print-nucleotide-count [dnastring]
 	(let [counts (count-nucleotides dnastring)]
 		(println (join " " (map #(.toString (second %)) (sort counts))))))
