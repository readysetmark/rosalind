; Mark Williams
; 2012
;
; Rosalind - SUBS
; Finding a Motif in DNA

(ns rosalind.subs
	(:use [clojure.java.io :only [reader]])
	(:use [clojure.string :only [join]]))

(defn whole-numbers [] (iterate inc 1))

(defn all-substring-locations [dna sub]
	(map first
		(remove #(not (second %))
			(partition 2 (interleave (whole-numbers)
				 	 	 (map #(= % sub) (map (partial apply str) (partition (.length sub) 1 dna))))))))

(defn from-file [filename]
	(with-open [rdr (reader filename)]
		(let [fileseq (line-seq rdr)
			  dna (first fileseq)
			  sub (second fileseq)]
			(println (join " " (all-substring-locations dna sub))))))
