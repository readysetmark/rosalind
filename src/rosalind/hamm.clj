; Mark Williams
; Nov 2012
;
; Rosalind - HAMM
; Counting Point Mutations

(ns rosalind.hamm
	(:use [clojure.java.io :only [reader]]))

(defn hamming-distance 
	([dna1 dna2]
		;(count (filter (partial apply not=) (partition 2 (interleave dna1 dna2)))))
		(count (remove identity (map = dna1 dna2))))
	([filename]
		(with-open [rdr (reader filename)]
			(let [dnaseq (line-seq rdr)
				  dna1 (first dnaseq)
				  dna2 (second dnaseq)]
				(hamming-distance dna1 dna2)))))
