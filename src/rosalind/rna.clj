; Mark Williams
; 2012
;
; Rosalind - RNA
; RNA Transcription

(ns rosalind.rna)

(defn transcribe-dna-to-rna [dnastring]
	(apply str (map #(if (= % \T) \U %) dnastring)))
