; Mark Williams
; 2012
;
; Rosalind - GC
; GC Content

(ns rosalind.gc
	(:use [clojure.java.io :only [reader]]))

(defn gc-content [dnastring]
	(let [dna-seq (seq dnastring)]
		(/  (count (filter #{\C \G} dna-seq))
			(double (count dna-seq)))))

(defn percent-format [percent]
	(str (* percent 100) "%"))

(defn to-dna [[[label] dna]]
	(let [name (.substring label 1)
		  dnastring (apply str dna)]
		{:name name :dnastring dnastring :gc-content (gc-content dnastring)}))

(defn dna-string-seq [lineseq]
	(let [is-label? #(= \> (first %))]
		(map to-dna (partition 2 (partition-by is-label? lineseq)))))

(defn find-highest-gc-content [filename]
	(with-open [rdr (reader filename)]
		(first (sort-by :gc-content > (dna-string-seq (line-seq rdr))))))

(defn print-highest-gc-content [filename]
	(let [dna (find-highest-gc-content filename)]
		(println (str (:name dna) "\r\n" (percent-format (:gc-content dna))))))
