; Mark Williams
; 2012
;
; Rosalind - REVC
; Reverse Complement

(ns rosalind.revc)

(defn dna-reverse-complement [dnastring]
	(letfn [(complement [symbol]
				(cond (= \A symbol) \T
					  (= \C symbol) \G
					  (= \G symbol) \C
					  :else \A))]
		(apply str (map complement (reverse dnastring)))))
